<!DOCTYPE html>
<html>
	<head>
		<title>SplitBD v1.1</title>
		<script type="text/javascript" src="js/jquery.js"></script>
	</head>

	<body>

		<form id="form">
			
			<div class="select" id="database">
				<label for="selectDB">Banco de Dados:</label>
				<select name="selectDB" id="selectDatabase">
				<option value="">SELECIONE UM .SQL</option>
				<?php
					$dir = dir("db/"); 
					while($file = $dir->read()):
						if(!is_dir($file)):
				?>
					<option value="<?php echo $file ?>"><?php echo $file ?></option>
				<?php
					endif; endwhile;
					$dir->close();
				?>
				</select>
			</div>

			<div class="select" id="database">
				<label for="splitType">Tipo de Split:</label>
				<select name="splitType" id="splitType">
					<option value="mult">ARQUIVOS SEPARADO</option>
					<option value="one">ARQUIVO UNICO</option>
				</select>
			</div>

			<div class="select" id="database">
				<label for="splitType">Tabelas:</label>
				<select name="selectTable" id="selectTable">
					<option value="">SELECIONE</option>
					<option value="all">TODAS TABELAS</option>
					<option value="select">SELECIONAR</option>
				</select>
			</div>

			<div class="select" id="database">
				<label for="splitType">Limpar Diretorio Saida:</label>
				<select name="clear" id="clear">
					<option value="1">Sim</option>
					<option value="0">Nao</option>
				</select>
			</div>

			<div id="tables" style="display: block;height: 350px;overflow: scroll;border: 1px solid #ccc;margin: 25px;">
				<button id="selectAll">Marcar Todos</button>
				<button id="deselectAll">Desmarcar Todos</button>
				<ul></ul>
			</div>

			<button id="execute">Executar</button>
			<ul class="info"></ul>
		</form>


	<script type="text/javascript">

		$(document).ready(function(){

			$("#execute").prop('disabled', true);
			$("#execute").on('click', function(e){
				e.preventDefault();
				$(".info").html("");
				$(this).prop('disabled', true);
				$(".info").append("<li>Iniciando...</li>");
				$(".info").append("<li>Salvando Configuracao...</li>");
				$.ajax({
					url: "script/saveSettings.php",
					method: "POST",
					data: $("#form").serialize(),
					success: function(r){
						if(r == "true"){
							$(".info").append("<li>Configuracao SALVA...</li>");

							var type = $("#splitType").val();
							var name = type=="one" ? "Arquivo Unico" : "Arquivos Separados";
							var url = "script/splitSql_"+type+".php";

							$(".info").append("<li>Executando Split ("+name+")...</li>");
							var start = new Date().getTime();
							$.ajax({
								url: url,
								success: function(r){
									var end = new Date().getTime();
									if(r == ""){
										$(".info").append("<li>Split executado com SUCESSO... ("+((end-start)/1000)+"s)</li>");
									}else{
										$(".info").append("<li>ERRO AO EXECUTAR SPLIT...</li>");
									}
									$("#execute").prop('disabled', false);
								}

							})
						}else{
							$(".info").append("<li>ERRO AO SALVAR CONFIGURACAO...</li>");
							$("#execute").prop('disabled', false);
						}
					}

				})


			})

			$("#selectAll").on('click', function(e){
				e.preventDefault();
				$('.tableSelect').each(function(){
					$(this).prop('checked',true);
				})
			})

			$("#deselectAll").on('click', function(e){
				e.preventDefault();
				$('.tableSelect').each(function(){
					$(this).removeAttr('checked', false);
				})
			})

			$("#tables").hide();

			$("#selectTable").on('change', function(){
				$("#tables ul").html("");
				if($(this).val() != "all"){
					$.ajax({
						url: "script/getNameTables.php",
						method: "POST",
						data: 'name='+$("#selectDatabase").val(),
						success: function(r){
							var tables = jQuery.parseJSON(r);
							var innerHtml = "";
							for(var i = 0; i < tables.length; i++){
								innerHtml += "<li>";
								innerHtml += "<label><input class='tableSelect' type='checkbox' name='allowTables[]' value="+tables[i]+" />" + tables[i]+"</label>";
								innerHtml += "</li>";
							}
							$("#tables ul").html(innerHtml);
							$("#tables").show();
							$("#execute").prop('disabled', false);
						}

					})
				}else{
					$("#tables").hide();
					$('.tableSelect').each(function(){
						$(this).removeAttr('checked', false);
					})
					$("#execute").prop('disabled', false);
				}
			})

			
			

		});

	</script>

	</body>
</html>