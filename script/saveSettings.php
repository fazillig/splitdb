<?php
	
	$pre_config = array(

		"dbName" => $_POST['selectDB'],

		"outFolder" => "splitTables",

		"clearOut" => $_POST['clear'],

		"onlyInsert" => false,

		"allowTables" => (isset($_POST['allowTables']) ? $_POST['allowTables'] : array())

	);

	file_put_contents("../config.php", "<?php\n \$config = " . var_export($pre_config, true) . ";");

	echo "true";
