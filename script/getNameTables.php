<?php

	$nameFile = "../db/" . $_POST['name'];
	
	$fileRead = fopen($nameFile,"r");
	$namesTable = array();


	while (!feof($fileRead)) {
		$line = fgets($fileRead);
		

		preg_match('/^DROP TABLE.*[`\'"](.*)[`\'"];$/', $line, $mDrop);
		if(count($mDrop) > 0 AND !in_array($mDrop[1], $namesTable)){
			$namesTable[] = $mDrop[1];
		}

		preg_match('/^CREATE TABLE.*[`\'"](.*)[`\'"]/', $line, $mCreate);
		if(count($mCreate) > 0 AND !in_array($mCreate[1], $namesTable)){
			$namesTable[] = $mCreate[1];
		}

		preg_match('/^INSERT INTO.*[`\'"](.*)[`\'"].*/U', $line, $mInsert);
		if(count($mInsert) > 0 AND !in_array($mInsert[1], $namesTable)){
			$namesTable[] = $mInsert[1];
		}

		
	}

	echo json_encode($namesTable);


