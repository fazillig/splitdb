<?php

	include "../config.php";

	$nameFile = "../db/" . $config['dbName'];
	$outFolder = "../" . $config['outFolder'];

	$table_allow =  $config['allowTables'];

	if(!file_exists($outFolder)){
		mkdir($outFolder);
	}else{
		if($config['clearOut'] == 1){
			$files = array_diff(scandir($outFolder), array('.','..'));
			foreach ($files as $file) {
				unlink($outFolder . "/". $file);
			}
		}
	}

	$fileRead = fopen($nameFile,"r");

	$openFile = fopen($outFolder."/splitTables.sql", "a");;
	$inCreate = false;
	$inInsert = false;


	while (!feof($fileRead)) {
		$line = fgets($fileRead);

		preg_match('/^DROP TABLE.*[`\'"](.*)[`\'"];$/', $line, $mDrop);
		if(count($mDrop) > 0 AND !$config['onlyInsert'] AND (in_array($mDrop[1], $table_allow) || count($table_allow) <= 0)){
			fwrite($openFile, $line);
		}

		preg_match('/^CREATE TABLE.*[`\'"](.*)[`\'"]/', $line, $mCreate);
		if(count($mCreate) > 0 AND !$config['onlyInsert'] AND (in_array($mCreate[1], $table_allow) || count($table_allow) <= 0)){
			$inCreate = true;
		}

		preg_match('/^INSERT INTO.*[`\'"](.*)[`\'"].*/U', $line, $mInsert);
		if(count($mInsert) > 0 AND (in_array($mInsert[1], $table_allow) || count($table_allow) <= 0)){
			$inInsert = true;
		}

		if($inCreate || $inInsert){
			fwrite($openFile, $line);
		}

		preg_match('/^.*\)(.*);$/', $line, $mClose);
		if(count($mClose) > 0){
			$inCreate = false;
			$inInsert = false;
		}

	}

	fclose($openFile);


